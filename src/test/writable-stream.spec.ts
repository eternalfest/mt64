import chai from "chai";

import { Mt64WritableStream } from "../lib/index.js";

describe("writable-stream", () => {
  describe("Mt64WritableStream", () => {
    interface TestItem {
      name: string;
      writes: ((stream: Mt64WritableStream) => any)[];
      expected: string;
    }

    const testItems: TestItem[] = [
      {
        name: "empty",
        writes: [],
        expected: "",
      },
      {
        name: "writeBoolBits(false)",
        writes: [(s: Mt64WritableStream) => s.writeBoolBits(false)],
        expected: "a",
      },
      {
        name: "writeBoolBits(true)",
        writes: [(s: Mt64WritableStream) => s.writeBoolBits(true)],
        expected: "G",
      },
      {
        name: "writeUint32Bits(1, 0)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(1, 0)],
        expected: "a",
      },
      {
        name: "writeUint32Bits(1, 1)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(1, 1)],
        expected: "G",
      },
      {
        name: "writeUint32Bits(2, 0)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(2, 0)],
        expected: "a",
      },
      {
        name: "writeUint32Bits(2, 1)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(2, 1)],
        expected: "q",
      },
      {
        name: "writeUint32Bits(2, 2)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(2, 2)],
        expected: "G",
      },
      {
        name: "writeUint32Bits(2, 3)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(2, 3)],
        expected: "W",
      },
      {
        name: "writeUint32Bits(3, 1)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(3, 1)],
        expected: "i",
      },
      {
        name: "writeUint32Bits(4, 1)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(4, 1)],
        expected: "e",
      },
      {
        name: "writeUint32Bits(5, 1)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(5, 1)],
        expected: "c",
      },
      {
        name: "writeUint32Bits(6, 0)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(6, 0)],
        expected: "a",
      },
      {
        name: "writeUint32Bits(6, 1)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(6, 1)],
        expected: "b",
      },
      {
        name: "writeUint32Bits(6, 2)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(6, 2)],
        expected: "c",
      },
      {
        name: "writeUint32Bits(6, 3)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(6, 3)],
        expected: "d",
      },
      {
        name: "writeUint32Bits(7, 1)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(7, 1)],
        expected: "aG",
      },
      {
        name: "writeUint32Bits(32, 0xffffffff)",
        writes: [(s: Mt64WritableStream) => s.writeUint32Bits(32, 0xffffffff)],
        expected: "_____W",
      },
    ];

    for (const {name, writes, expected} of testItems) {
      it(name, () => {
        const stream: Mt64WritableStream = new Mt64WritableStream();
        for (const write of writes) {
          write(stream);
        }
        const actual: string = stream.getBytes().toString("utf8");
        chai.assert.strictEqual(actual, expected);
      });
    }
  });
  describe("Mt64WritableStream - Errors", () => {
    it("Throws a BitOverflow error if writing more than 32 bits", () => {
      const stream: Mt64WritableStream = new Mt64WritableStream();
      chai.assert.throw(() => stream.writeUint32Bits(33, 0));
    });
  });
});
