import chai from "chai";

import { Mt64ReadableStream } from "../lib/index.js";

describe("readable-stream", () => {
  describe("Mt64WritableStream", () => {
    interface TestItem {
      name: string;
      input: string;
      reads: ([(stream: Mt64ReadableStream) => any, any])[];
    }

    const testItems: TestItem[] = [
      {
        name: "empty",
        input: "",
        reads: [],
      },
      {
        name: "a - readBoolBits()",
        input: "a",
        reads: [[(s: Mt64ReadableStream) => s.readBoolBits(), false]],
      },
      {
        name: "G - readBoolBits()",
        input: "G",
        reads: [[(s: Mt64ReadableStream) => s.readBoolBits(), true]],
      },
      {
        name: "a - readUint32Bits(1)",
        input: "a",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(1), 0]],
      },
      {
        name: "G - readUint32Bits(1)",
        input: "G",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(1), 1]],
      },
      {
        name: "a - readUint32Bits(2)",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(2), 0]],
        input: "a",
      },
      {
        name: "q - readUint32Bits(2)",
        input: "q",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(2), 1]],
      },
      {
        name: "G - readUint32Bits(2)",
        input: "G",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(2), 2]],
      },
      {
        name: "W - readUint32Bits(2)",
        input: "W",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(2), 3]],
      },
      {
        name: "i - readUint32Bits(3)",
        input: "i",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(3), 1]],
      },
      {
        name: "e - readUint32Bits(4)",
        input: "e",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(4), 1]],
      },
      {
        name: "c - readUint32Bits(5)",
        input: "c",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(5), 1]],
      },
      {
        name: "a - readUint32Bits(6)",
        input: "a",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(6), 0]],
      },
      {
        name: "b - readUint32Bits(6)",
        input: "b",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(6), 1]],
      },
      {
        name: "c - readUint32Bits(6)",
        input: "c",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(6), 2]],
      },
      {
        name: "d - readUint32Bits(6)",
        input: "d",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(6), 3]],
      },
      {
        name: "aG - readUint32Bits(7)",
        input: "aG",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(7), 1]],
      },
      {
        name: "_____W - readUint32Bits(32)",
        input: "_____W",
        reads: [[(s: Mt64ReadableStream) => s.readUint32Bits(32), 0xffffffff]],
      },
    ];

    for (const {name, input, reads} of testItems) {
      it(name, () => {
        const buffer: Buffer = Buffer.from(input);
        const stream: Mt64ReadableStream = new Mt64ReadableStream(buffer);
        for (const [read, expected] of reads) {
          const actual: any = read(stream);
          chai.assert.strictEqual(actual, expected);
        }
      });
    }
  });

  describe("Mt64ReadableStream - Errors", () => {
    it("Throws a BitOverflow error if reading more than 32 bits", () => {
      const stream: Mt64ReadableStream = new Mt64ReadableStream(Buffer.from("aaaaaaa"));
      chai.assert.throw(() => stream.readUint32Bits(33));
    });
  });
});
