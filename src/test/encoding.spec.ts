import chai from "chai";

import { isMt64Char, mt64CharFromValue, mt64CharToValue } from "../lib/index.js";

describe("encoding", () => {
  describe("mt64CharFromValue", () => {
    interface TestItem {
      input: number;
      expected: string | undefined;
    }

    const testItems: TestItem[] = [
      {
        input: 0,
        expected: "a",
      },
      {
        input: 1,
        expected: "b",
      },
      {
        input: 63,
        expected: "_",
      },
      {
        input: 64,
        expected: undefined,
      },
      {
        input: -1,
        expected: undefined,
      },
    ];

    for (const {input, expected} of testItems) {
      it(input.toString(10), () => {
        const actual: string | undefined = mt64CharFromValue(input);
        chai.assert.strictEqual(actual, expected);
      });
    }
  });

  describe("mt64CharToValue", () => {
    interface TestItem {
      input: string;
      expected: number | undefined;
    }

    const testItems: TestItem[] = [
      {
        input: "a",
        expected: 0,
      },
      {
        input: "b",
        expected: 1,
      },
      {
        input: "_",
        expected: 63,
      },
      {
        input: "$",
        expected: undefined,
      },
    ];

    for (const {input, expected} of testItems) {
      it(input, () => {
        const actual: number | undefined = mt64CharToValue(input);
        chai.assert.strictEqual(actual, expected);
      });
    }
  });

  describe("isMt64Char", () => {
    interface TestItem {
      input: string;
      expected: boolean;
    }

    const testItems: TestItem[] = [
      {
        input: "a",
        expected: true,
      },
      {
        input: "b",
        expected: true,
      },
      {
        input: "_",
        expected: true,
      },
      {
        input: "$",
        expected: false,
      },
    ];

    for (const {input, expected} of testItems) {
      it(input, () => {
        const actual: boolean = isMt64Char(input);
        chai.assert.strictEqual(actual, expected);
      });
    }
  });
});
