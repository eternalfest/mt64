import { Incident } from "incident";
import { Uint6, Uint32, UintSize } from "semantic-types";

import { mt64CodePointToValue } from "./encoding.js";

const BITS_PER_BYTE: number = 6;

export class Mt64ReadableStream {
  private bytes: Uint8Array;
  private bytePos: UintSize;
  private bitPos: UintSize;

  constructor(bytes: Uint8Array, bitOffset: UintSize = 0) {
    this.bytes = bytes;
    this.bytePos = 0;
    this.bitPos = bitOffset;
  }

  readBoolBits(): boolean {
    return this.readUintBits(1) > 0;
  }

  readUint32Bits(n: UintSize): Uint32 {
    return this.readUintBits(n);
  }

  private readUintBits(n: number): number {
    if (n > 32) {
      // Even if we could read up to 53 bits, we restrict it to 32 bits (which
      // must already be handled carefully)
      throw new Incident("BitOverflow", "Cannot read above 32 bits at once");
    }
    let result: number = 0;
    while (n > 0) {
      if (this.bitPos + n < BITS_PER_BYTE) {
        const endBitPos: number = this.bitPos + n;
        const shift: number = 1 << (endBitPos - this.bitPos);
        const byte: Uint6 = this.peekByte();
        const cur: number = (byte >>> BITS_PER_BYTE - endBitPos) & (shift - 1);
        result = result * shift + cur;
        n = 0;
        this.bitPos = endBitPos;
      } else {
        const shift: number = 1 << (BITS_PER_BYTE - this.bitPos);
        const byte: Uint6 = this.peekByte();
        const cur: number = byte & (shift - 1);
        result = result * shift + cur;
        n -= (BITS_PER_BYTE - this.bitPos);
        this.bitPos = 0;
        this.bytePos++;
      }
    }
    return result;
  }

  private peekByte(): Uint6 {
    const codePoint: number = this.bytes[this.bytePos];
    return mt64CodePointToValue(codePoint)!;
  }
}
