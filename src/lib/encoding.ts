/**
 * This module defines the MT64 encoding.
 */

import { Uint6 } from "semantic-types";

const ALPHABET: string = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";
const CHAR_TO_VALUE: Map<string, Uint6> = new Map([...ALPHABET].map((c, i) => [c, i] as [string, Uint6]));

export function mt64CharFromValue(value: Uint6): string | undefined {
  return ALPHABET[value];
}

export function mt64CharToValue(char: string): Uint6 | undefined {
  return CHAR_TO_VALUE.get(char);
}

export function mt64CodePointFromValue(value: Uint6): number | undefined {
  return ALPHABET.codePointAt(value);
}

export function mt64CodePointToValue(char: number): Uint6 | undefined {
  return CHAR_TO_VALUE.get(String.fromCodePoint(char));
}

export function isMt64Char(char: string): boolean {
  return CHAR_TO_VALUE.has(char);
}
