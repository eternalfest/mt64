import { Incident } from "incident";
import { Uint6, Uint32, UintSize } from "semantic-types";

import { mt64CodePointFromValue } from "./encoding.js";

const BITS_PER_BYTE: number = 6;

export class Mt64WritableStream {
  private bitPos: UintSize;
  private bitsBuffer: Uint6;
  private chunks: number[];

  constructor() {
    this.bitPos = 0;
    this.bitsBuffer = 0;
    this.chunks = [];
  }

  writeBoolBits(value: boolean): void {
    this.writeUintBits(1, value ? 1 : 0);
  }

  writeUint32Bits(n: UintSize, value: Uint32): void {
    this.writeUintBits(n, value);
  }

  align(): void {
    if (this.bitPos !== 0) {
      this.writeByte(this.bitsBuffer);
      this.bitPos = 0;
      this.bitsBuffer = 0;
    }
  }

  getBytes(): Buffer {
    this.align();
    return Buffer.from(this.chunks);
  }

  private writeUintBits(bits: number, value: number): void {
    if (bits > 32) {
      // Even if we could write more bits, we restrict it to 32 bits (which
      // must already be handled carefully)
      throw new Incident("BitOverflow", "Cannot write above 32 bits at once");
    }
    while (bits > 0) {
      const availableBits: number = BITS_PER_BYTE - this.bitPos;

      const consumedBits: number = Math.min(availableBits, bits);
      const chunk: number = (value >>> (bits - consumedBits)) & ((1 << consumedBits) - 1);
      this.bitsBuffer = this.bitsBuffer | (chunk << (availableBits - consumedBits));
      bits -= consumedBits;
      this.bitPos += consumedBits;
      if (this.bitPos === BITS_PER_BYTE) {
        this.writeByte(this.bitsBuffer);
        this.bitsBuffer = 0;
        this.bitPos = 0;
      }
    }
  }

  private writeByte(value: Uint6) {
    const codepoint: number = mt64CodePointFromValue(value)!;
    this.chunks.push(codepoint & 0xff);
  }
}
