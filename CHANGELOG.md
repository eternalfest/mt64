# 2.0.0 (2021-07-24)

- **[Breaking change]** Drop `lib` prefix and `.js` extension from deep-imports.
- **[Internal]** Refactor project structure.

# 1.0.1 (2019-12-28)

- **[Fix]** Update dependencies.
- **[Fix]** Update CI image.

# 1.0.0 (2019-05-15)

- **[Fix]** Update dependencies.
- **[Internal]** Fix CI script.

# 0.2.1 (2018-12-03)

- **[Fix]** Update dependencies.

# 0.2.0 (2018-10-11)

- **[BREAKING CHANGE]** Return bytes when collecting the result of a writable stream.
- **[Feature]** Implement readable stream.

# 0.1.1 (2018-10-11)

- **[Feature]** Expose `isMt64Char`, `mt64FromValue` and `mt64ToValue`.

# 0.1.0 (2018-10-11)

- **[Feature]** First release
